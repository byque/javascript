/**
 * Transforma de bits por segundo (bps) a mega bits por segundo (Mbps)
 * @param {float} bps - Velocidad [bps]
 * @returns {float} mbps - Velocidad [Mbps]
 */

module.exports = {
  bpsAMbps: function(bps) {
    var mbps = Math.round(bps/10000)/100;
    return mbps.toFixed(2);
  }
}
