/**
 * Transforma de bits por segundo (bps) a mega bits por segundo (Mbps)
 * @param {float} bps - Velocidad [bps]
 * @returns {float} mbps - Velocidad [Mbps]
 */
function bpsAMbps(bps) {
  var mbps = Math.round(bps/10000)/100;
  return mbps.toFixed(2);
}
