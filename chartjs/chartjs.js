function demoCuadro() {
  var ctx = document.getElementById('miCuadro').getContext('2d');
  var chart = new Chart(ctx, {
    // El tipo de cuadro a crear
    type: 'line',

    // Los datos de un conjunto de datos
    data: {
      labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio'],
      datasets: [{
        label: 'Mi primer conjunto de datos',
        backgroundColor: 'rgb(150, 213, 97)',
        borderColor: 'rgb(150, 99, 132)',
        data: [0, 10, 5, 2, 20, 30, 45]
      }]
    },

    // Opciones de configuración
    options: {}
  });
}
